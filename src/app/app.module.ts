import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../services/product.service';
import {FormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {RequestCacheService} from '../services/request-cache.service';
import {CachingInterceptorService} from '../services/caching-interceptor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    NgxPaginationModule
  ],
  providers: [
    RequestCacheService,
    {provide: HTTP_INTERCEPTORS, useClass: CachingInterceptorService, multi: true},
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
