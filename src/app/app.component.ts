import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NgbModalConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../services/product.service';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class AppComponent {
  public prducts: any[];
  public categorys: any[];
  errorMessage: any;
  p: number = 1;
  selected;
  isActive = false;
  product: any;
  public cacheproducts: any[];

  constructor(private productservice: ProductService) {
    this.productservice.getproducts()
      .subscribe(products => {
        // @ts-ignore
        this.prducts = products;
        this.cacheproducts = this.prducts;
      }, error => this.errorMessage = <any>error);

    this.productservice.getCat()
      .subscribe(cat => {
        // @ts-ignore
        this.categorys = cat;
      }, error => this.errorMessage = <any>error);
  }

  //filtring on dropdown select
  onSelect(val: any) {
    if (val == '0')
      this.prducts = this.cacheproducts;
    else
      this.prducts = this.cacheproducts.filter((item) => item.category == val);
  }

  //calculate the average
  getAvg(index: number): number {
    this.product = this.prducts.find(x => x._id == index);
    let Avg = 0;
    let sum = 0;
    for (let i = 0; i < this.product.reviews.length; i++) {
      sum += this.product.reviews[i].rating;
    }
    Avg = sum / this.product.reviews.length;
    return Avg;
  }
}
